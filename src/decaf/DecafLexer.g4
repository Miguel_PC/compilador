lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

TK_class : 'class Program';

LCURLY : '{';
RCURLY : '}';

LPARA : '(';
RPARA : ')';

LCOLCHETE : '[';
RCOLCHETE : ']';

IF: 'if';
PRES1 : 'boolean';
PRES3 : 'callout';
PRES4 : 'class';
PRES6: 'else';
PRES9 : 'int';
PRES10 : 'return';
PRES12: 'void';
PRES8 : 'for';
PRES2 : 'break';
PRES5 : 'continue';

PRES11: 'true';
PRES7: 'false';

SOMA: '+';
SUBTRACAO: '-';
MULTIPLICACAO: '*';
DIVISAO: '/';
MODULO: '%';

MAIORQ: '>';
MENORQ: '<';
MAIORIGUAL: '>=';
MENORIGUAL: '<=';

COMPARACAO: '==';
DIFERENTE: '!=';

ECOMERCIAL: '&&';
OULOGICO: '||';

MAISIGUAL: '+=';
MENOSIGUAL: '-=';

IGUAL: '=';

PONTU: (';'|'!'|',');

PY: '|';

IDENTIFICADOR: ('a'..'z' | 'A'..'Z' | '_' | '%' )+  ([0-9])* IDENTIFICADOR?;

INT_LITERAL: [0-9]+ ('x' ([a-fA-F] | [0-9])+)?;

WS_ : (' ' | '\n' | '\t') -> skip;
SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHAR : '\'' ( [ !#-&(-.0-Z^-~] | INTEIRO | ESC) '\'';

STRING_LITERAL : '"' (IDENTIFICADOR | PONTUACAO)+ '"';

fragment INTEIRO: [0-9];
fragment PONTUACAO: ( '.' | '?' | ',' | ';' | ' ' | ':'| '!' | ESPECIAL);
fragment ESPECIAL: '\\' ( '\'' | '\"' | '\\' | IDENTIFICADOR );
fragment ESC :  '\\' ('n'|'t'|'\\'|'"');
