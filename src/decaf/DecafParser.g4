parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

program: TK_class LCURLY (var_decl+)* method_decl* RCURLY EOF ;

method_decl: (type | PRES12) IDENTIFICADOR LPARA ((var_decl)+ PONTU*)? RPARA block;

block: LCURLY var_decl* statement* RCURLY;

var_decl: type (IDENTIFICADOR PONTU* )* array_decl? PONTU*;

array_decl: LCOLCHETE INT_LITERAL RCOLCHETE;

type: PRES9 | PRES1;

statement: location assign_op expr PONTU
	| method_call PONTU
	| IF LPARA expr RPARA block (PRES6 block)?
	| PRES8 IDENTIFICADOR IGUAL expr PONTU expr block
	| PRES10 expr? PONTU
	| PRES2 PONTU
	| PRES5 PONTU
	| block;

assign_op: IGUAL 
	| MAISIGUAL 
	| MENOSIGUAL ;

method_call: IDENTIFICADOR LPARA expr (PONTU expr)* RPARA 
	| PRES3 LPARA STRING_LITERAL (PONTU* (expr | STRING_LITERAL)+ PONTU*)? RPARA;

location: IDENTIFICADOR 
	| IDENTIFICADOR LCOLCHETE expr RCOLCHETE;

expr: location
	| method_call
	| identifier_decl
	| expr bin_op expr
	| bin_op expr*
	| PONTU expr*
	| LPARA expr* RPARA;

bin_op: arith_op 
	| rel_op 
	| eq_op 
	| cond_op;

arith_op: SOMA 
	| SUBTRACAO 
	| MULTIPLICACAO 
	| DIVISAO 
	| MODULO;

rel_op: MENORQ 
	| MAIORQ 
	| MENORIGUAL 
	| MAIORIGUAL;

eq_op: COMPARACAO 
	| DIFERENTE;

cond_op: ECOMERCIAL 
	| OULOGICO;

identifier_decl: int_literal int_literal* 
	| int_literal 
	| char_literal 
	| bool_literal;

int_literal: INT_LITERAL;

char_literal: CHAR;

bool_literal: PRES11 
	| PRES7;


